from flask import Flask
from flask_cors import CORS

from .routes.index import index
from .routes.search import search
from .util.format_response import Response

app = Flask(__name__)
app.url_map.strict_slashes = False
CORS(app, resources={r"/*": {"origins": "*"}})

app.register_blueprint(index, url_prefix="/")
app.register_blueprint(search, url_prefix="/api")


@app.errorhandler(400)
def bad_request(e):
    return Response().send(
        data={"error": str(e)},
        message="Alguns campos obrigatórios para essa requisição não foram informados no corpo.",
        code="bad_request",
        status=400,
    )


@app.errorhandler(404)
def not_found(e):
    return Response().send(
        data={"error": str(e)},
        message="Ops, a rota informada não existe.",
        code="route_not_found",
        status=404,
    )


@app.errorhandler(500)
def internal_error(e):
    return Response().send(
        data={"error": str(e)},
        message="Desculpe, tivemos um erro interno ao processar sua requisição.",
        code="internal_error",
        status=500,
    )
