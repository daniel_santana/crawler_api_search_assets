from flask import Blueprint, request

from ..controller.search_asset import SearchAsset
from ..controller.deep_crawler import DeepCrawler
from ..util.format_response import Response

search = Blueprint("search", __name__)


@search.after_request
def after_request(response):
    if request.method not in ["OPTIONS", "HEAD"]:
        print(request)
    return response


@search.route("/search/<asset>", methods=["GET"])
def get_assets_query(asset):
    query_result = SearchAsset.search_asset_crawler(asset=asset)

    return Response().send(
        data=query_result
    )


@search.route("/resume/company/<asset>", methods=["GET"])
def get_resume_company(asset):
    query_result = DeepCrawler().resume_company(asset=asset)
    return Response().send(
        data=query_result
    )


@search.route("/balancos/receitaliquida/chart/<asset>/<year>/<period>", methods=["GET"])
def get_balancos_receitaliquida(asset, year, period):
    query_result = SearchAsset.search_asset_detail_receitaliquida_chart(asset=asset, year=year, period=period)

    return Response().send(
        data=query_result
    )


@search.route("/balancos/ativospassivos/chart/<year>/<period>", methods=["GET"])
def get_balancos_ativospassivos(year, period):
    query_result = SearchAsset.search_asset_detail_ativospassivos_chart(year=year, period=period)

    return Response().send(
        data=query_result
    )


@search.route("/balancos/balancopatrimonial/chart/<asset>/<period>", methods=["GET"])
def get_balancos_balancopatrimonial(asset, period):
    query_result = SearchAsset.search_asset_detail_balancopatrimonial_chart(asset=asset, period=period)

    return Response().send(
        data=query_result
    )


@search.route("/balancos/balancoresultados/chart/<asset>/<period>", methods=["GET"])
def get_balancos_balancoresultados(asset, period):
    query_result = SearchAsset.search_asset_detail_balancoresultados_chart(asset=asset, period=period)

    return Response().send(
        data=query_result
    )


@search.route("/cotacoes/acao/chart/<asset>/<period>", methods=["GET"])
def get_balancos_detail_historico(asset, period):
    query_result = SearchAsset.search_asset_detail_historico_chart(asset=asset, period=period)

    return Response().send(
        data=query_result
    )
