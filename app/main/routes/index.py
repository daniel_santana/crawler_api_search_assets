from datetime import datetime

from flask import Blueprint
from flask_cors import cross_origin

from ..util.format_response import Response

index = Blueprint("index", __name__)


@index.route("", methods=["GET", "OPTIONS"])
@cross_origin(origin="*", allow_headers=["Content-Type"])
def get_status_api():
    return Response().send(
        code="success",
        status=200,
        message="Data e hora atual: {}".format(datetime.now().isoformat()),
    )
