import re

from logzero import logger
from datetime import datetime
from bs4 import BeautifulSoup
from user_agent import generate_user_agent


import settings
from ..util.requests import Requests


class DeepCrawler:
    @staticmethod
    def __get_page_data(asset) -> str:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/acoes/{asset}", headers=headers
        )
        return _response.text

    def __extract_biography(self, soup) -> list:
        paragraphs = []
        for h2 in soup.findAll('h2'):
            h2.decompose()
        _txt = soup.find("div", {"class": "biography"})
        for p in _txt.findAll('p'):
            paragraphs.append(p.get_text().strip())
        return paragraphs

    def __extract_relevant_points(self, soup) -> list:
        _txt = soup.find("div", {"id": "relevant_points"})
        _relevant_points = []
        for element in _txt.findAll("li"):
            _relevant_points.append(element.get_text().strip())
        return _relevant_points

    def __extract_basic_info(self, soup) -> dict:
        _txt = soup.find("div", {"id": "data_about"})
        _td = _txt.findAll("td")
        _tags = _txt.findAll("div", {"class": "tag-ticker"})
        return {
            "companyName": _td[1].get_text(),
            "document": _td[3].get_text(),
            "debutStockExchange": _td[5].get_text(),
            "numberEmployees": _td[7].get_text(),
            "companyFoundation": _td[9].get_text(),
            "tickers": [el.get_text().strip() for el in _tags]
        }

    def __extract_about_number_company(self, soup) -> dict:
        _txt = soup.find("div", {"id": "table-indicators-company"})
        _cell_value = _txt.findAll("span", {"class": "value"})
        _simple_value = _txt.findAll("div", {"class": "simple-value"})
        _detail_value = _txt.findAll("div", {"class": "detail-value"})

        return {
            "valorMercado": {
                "simpleValue": _simple_value[0].get_text().strip(),
                "detailValue": _detail_value[0].get_text().strip()
            },
            "valorFirma": {
                "simpleValue": _simple_value[1].get_text().strip(),
                "detailValue": _detail_value[1].get_text().strip()
            },
            "patrimonioLiquido": {
                "simpleValue": _simple_value[2].get_text().strip(),
                "detailValue": _detail_value[2].get_text().strip()
            },
            "totalPapeis": {
                "simpleValue": _simple_value[3].get_text().strip(),
                "detailValue": _detail_value[3].get_text().strip()
            },
            "ativos": {
                "simpleValue": _simple_value[4].get_text().strip(),
                "detailValue": _detail_value[4].get_text().strip()
            },
            "ativoCirculante": {
                "simpleValue": _simple_value[5].get_text().strip(),
                "detailValue": _detail_value[5].get_text().strip()
            },
            "dividaBruta": {
                "simpleValue": _simple_value[6].get_text().strip(),
                "detailValue": _detail_value[6].get_text().strip()
            },
            "dividaLiquida": {
                "simpleValue": _simple_value[7].get_text().strip(),
                "detailValue": _detail_value[7].get_text().strip()
            },
            "disponibilidade": {
                "simpleValue": _simple_value[8].get_text().strip(),
                "detailValue": _detail_value[8].get_text().strip()
            },
            "segmentoListagem": _cell_value[9].get_text().strip(),
            "freeFloat": _cell_value[10].get_text().strip(),
            "tagAlong": _cell_value[11].get_text().strip()
        }


    def __extract_fundaments(self, soup) -> dict:
        _txt = soup.find("div", {"id": "indicators"})
        _table_indicators = _txt.find("div", {"id": "table-indicators"})
        _cell_value = _table_indicators.findAll("div", {"class": "value"})
        _card_cotacao = soup.find("div", {"class": "cotacao"})
        _last_price = _card_cotacao.find("div", {"class": "_card-body"})

        _card_pl = soup.find("div", {"class": "pl"})
        _pl_value = _card_pl.find("div", {"class": "_card-body"})

        return {
            "lastPrice": _last_price.get_text().strip(),
            "valorizacao": _pl_value.get_text().strip(),
            "pl": _cell_value[0].get_text().strip(),
            "pvp": _cell_value[1].get_text().strip(),
            "dy": _cell_value[2].get_text().strip(),
            "payout": _cell_value[3].get_text().strip(),
            "margemLiquida": _cell_value[4].get_text().strip(),
            "margemBruta": _cell_value[5].get_text().strip(),
            "margemEbit": _cell_value[6].get_text().strip(),
            "margemEbitida": _cell_value[7].get_text().strip(),
            "evEbitida": _cell_value[8].get_text().strip(),
            "evEbit": _cell_value[9].get_text().strip(),
            "pEbitida": _cell_value[10].get_text().strip(),
            "pEbit": _cell_value[11].get_text().strip(),
            "pAtivo": _cell_value[12].get_text().strip(),
            "pCapGiro": _cell_value[13].get_text().strip(),
            "pAtivoCircLiq": _cell_value[14].get_text().strip(),
            "psr": _cell_value[15].get_text().strip(),
            "vpa": _cell_value[16].get_text().strip(),
            "lpa": _cell_value[17].get_text().strip(),
            "giroAtivos": _cell_value[18].get_text().strip(),
            "roe": _cell_value[19].get_text().strip(),
            "roic": _cell_value[20].get_text().strip(),
            "roa": _cell_value[21].get_text().strip(),
            "dividaLiquidaPatromonio": _cell_value[22].get_text().strip(),
            "dividaLiquidaEbitida": _cell_value[23].get_text().strip(),
            "dividaLiquidaEbit": _cell_value[24].get_text().strip(),
            "dividaBrutaPatrimonio": _cell_value[25].get_text().strip(),
            "patrimonioAtivos": _cell_value[26].get_text().strip(),
            "passivosAtivos": _cell_value[27].get_text().strip(),
            "liquidezCorrente": _cell_value[28].get_text().strip(),
            "cagrReceitas5Anos": _cell_value[29].get_text().strip(),
            "cagrLucros5Anos": _cell_value[30].get_text().strip()
        }

    def resume_company(self, asset: str) -> dict:
        _content = self.__get_page_data(asset=asset)
        _soup = BeautifulSoup(_content, "html.parser")
        _obj = {
            "relevantPoints": self.__extract_relevant_points(soup=_soup),
            "basicInfo": self.__extract_basic_info(soup=_soup),
            "companyValues": self.__extract_about_number_company(soup=_soup),
            "fundaments": self.__extract_fundaments(soup=_soup),
            "biography": self.__extract_biography(soup=_soup),
        }
        return _obj
