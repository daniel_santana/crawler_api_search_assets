from user_agent import generate_user_agent

import settings
from ..util.requests import Requests


class SearchAsset:

    @staticmethod
    def search_asset_detail_receitaliquida_chart(asset, year, period) -> dict:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/api/balancos/receitaliquida/chart/{asset}/{year}/{period}/", headers=headers
        )
        return _response.json()

    @staticmethod
    def search_asset_detail_ativospassivos_chart(year, period) -> dict:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/api/balancos/ativospassivos/chart/{year}/{period}/", headers=headers
        )
        return _response.json()

    @staticmethod
    def search_asset_detail_balancopatrimonial_chart(asset, period) -> dict:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/api/balancos/balancopatrimonial/chart/{asset}/{period}/", headers=headers
        )
        return _response.json()

    @staticmethod
    def search_asset_detail_balancoresultados_chart(asset, period) -> dict:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/api/balancos/balancoresultados/chart/{asset}/{period}/yearly/", headers=headers
        )
        return _response.json()

    @staticmethod
    def search_asset_detail_historico_chart(asset, period) -> dict:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(
            f"{settings.URL_CRAWLER}/api/cotacoes/acao/chart/{asset.lower()}/{period}/true/", headers=headers
        )
        return _response.json()

    @staticmethod
    def search_asset_crawler(asset: str) -> object:
        headers = {"User-Agent": generate_user_agent()}
        _response = Requests().get(f"{settings.URL_CRAWLER}/api/search/{asset}/", headers=headers)
        return _response.json()

