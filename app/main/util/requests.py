import requests
import requests_cache
from logzero import logger
from fake_useragent import UserAgent


class Requests(object):
    def __init__(self, name="cache", expiration=500):
        requests_cache.install_cache(cache_name=name, expire_after=expiration)
        self._get_session()

    def __del__(self):
        self.session.close()
        delattr(self, "_session")

    def _get_session(self):
        self._session = requests.Session()

    @property
    def _gerenate_user_agent(self) -> str:
        ua = UserAgent(cache=False)
        return ua.random

    @property
    def session(self):
        if hasattr(self, "_session"):
            self._get_session()
        return self._session

    @property
    def headers(self):
        self.session.headers.update({"User-Agent": self._gerenate_user_agent})
        return self.session.headers

    def get(self, url, **kwargs):
        response = self.session.get(url, **kwargs)
        if response.ok:
            logger.info(f"GET <{response.status_code}> [{url}]")
            return response

        logger.warning(f"GET <{response.status_code}> [{url}]")
        return {}

    def post(self, url, **kwargs):
        response = self.session.post(url, **kwargs)
        if response.ok:
            logger.info(f"POST <{response.status_code}> [{url}]")
            return response

        logger.warning(f"POST <{response.status_code}> [{url}]")
        return {}
