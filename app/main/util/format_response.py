import json

import bson
from flask import make_response


class Response:
    def __init__(self):
        self.headers = {
            "Content-Type": "application/json",
            "X-Request-ID": str(bson.objectid.ObjectId()),
        }

    def send(
        self,
        data=None,
        status=200,
        message: str = None,
        code: str = None,
        headers=None,
        others_data: dict = None,
    ):
        if status in [204]:
            response = ""
        else:
            if others_data is None:
                others_data = {}

            response = others_data
            response["status"] = status
            response["message"] = message
            response["code"] = code
            response["data"] = data

        return make_response(
            json.dumps(response), status, self.__format_headers(headers)
        )

    def __format_headers(self, headers=None) -> dict:
        if headers is None:
            headers = {}

        try:
            headers.update(
                dict(
                    (k.lower(), v)
                    for k, v in self.headers.items()
                    if k not in list(map(str.lower, headers.keys()))
                )
            )
        except Exception:
            headers = self.headers
        finally:
            return headers
