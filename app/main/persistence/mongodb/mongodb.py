import json

from logzero import logger
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

import settings
import app.main.util.encoder_object as encoder_obj
from app.main.util.singleton import Singleton


class MongoDB(object):
    def __init__(self):

        self._host = settings.MONGO_URL
        self._databse = settings.MONGO_DATABASE
        self._username = settings.MONGO_USERNAME
        self._password = settings.MONGO_PASSWORD
        self._conn = None

    def __enter__(self):
        self._conn = self.get_instance
        self.test_ping()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_connection()

    @property
    def connection_database(self):
        return MongoClient(
            host=self._host,
            username=self._username,
            password=self._password,
            authSource=self._databse,
            authMechanism="SCRAM-SHA-1",
            retryWrites=False,
        )

    @property
    def get_instance(self):
        try:
            rw = Singleton()
            rw.conn = self.connection_database
            return rw.conn
        except ConnectionFailure as e:
            logger.error(
                f"[MONGO DB] Connection is failure check paramenters of connection.", e
            )

    def test_ping(self):
        try:
            x = self._conn[self._databse].command("ping")
            return x.get("ok", "")
        except ConnectionFailure as e:
            logger.error(f"[MONGO DB] Test of connection is failure", e)

    def get_one(self, collection, **kwargs):
        try:
            query = kwargs.get("query", {})
            db = self._conn[self._databse]
            x = db[collection].find_one(query)
            rx_str = json.dumps(x, default=encoder_obj.new_encoder_object)
            return json.loads(rx_str)
        except Exception as e:
            logger.error(
                f"[MONGO DB] Occurred one error get data in database check.", e
            )

    def get_all(self, collection, **kwargs) -> list:
        try:
            data = []
            db = self._conn[self._databse]
            query = kwargs.get("query")
            limit = kwargs.get("limit", 0)
            x = []
            if query:
                x = db[collection].find(query, {"historical": {"$slice": limit}})
            else:
                x = db[collection].find()
            for row in x:
                data.append(row)
            rx_str = json.dumps(data, default=encoder_obj.new_encoder_object)
            return json.loads(rx_str)

        except Exception as e:
            logger.error(f"[MONGO DB] Occurred one error get all database.", e)

    def add_or_update(self, collection, **kwargs) -> str:
        try:
            data = kwargs.get("data")
            key = kwargs.get("key")
            db = self._conn[self._databse]
            x = db[collection].update(key, data, upsert=True)
            return x

        except Exception as e:
            logger.error(
                f"[MONGO DB] Occurred one error update/save data in database check.", e
            )

    def add_one(self, collection, **kwargs) -> str:
        try:
            data = kwargs.get("data")
            db = self._conn[self._databse]
            x = db[collection].insert_one(data)
            return x.inserted_id
        except Exception as e:
            logger.error(
                f"[MONGO DB] Occurred one error save data in database check.", e
            )

    def change_one(self, collection, **kwargs):
        try:
            query = kwargs.get("query")
            new_data = kwargs.get("new_data")
            db = self._conn[self._databse]
            db[collection].update_one(query, {"$set": new_data})

        except Exception as e:
            logger.error(
                f"[MONGO DB] Occurred one error update data in database check.", e
            )

    def close_connection(self):
        self._conn.close()
