import os

ENVIRONMENT = os.environ.get("ENVIRONMENT", "develop").lower()
SERVICE_NAME = os.environ.get("SERVICE_NAME", "service:cloud2data-api")

URL_CRAWLER = os.getenv("URL_CRAWLER", "https://investidor10.com.br")

API_HOST = os.environ.get("API_HOST", "0.0.0.0")
API_PORT = int(os.environ.get("PORT", 9090))
