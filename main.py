from logzero import logger

import settings
from app.main.api import app


if __name__ == '__main__':
    logger.info(f"Application is running.")

    app.run(
        host=settings.API_HOST,
        port=settings.API_PORT
    )
